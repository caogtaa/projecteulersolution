### 暴力
[0001](https://projecteuler.net/problem=1)&nbsp;&nbsp;&nbsp;&nbsp;[0002](https://projecteuler.net/problem=2)&nbsp;&nbsp;&nbsp;&nbsp;[0004](https://projecteuler.net/problem=4)&nbsp;&nbsp;&nbsp;&nbsp;[0005](https://projecteuler.net/problem=5)&nbsp;&nbsp;&nbsp;&nbsp;[0006](https://projecteuler.net/problem=6)&nbsp;&nbsp;&nbsp;&nbsp;[0008](https://projecteuler.net/problem=8)&nbsp;&nbsp;&nbsp;&nbsp;[0009](https://projecteuler.net/problem=9)&nbsp;&nbsp;&nbsp;&nbsp;[0011](https://projecteuler.net/problem=11)&nbsp;&nbsp;&nbsp;&nbsp;[0012](https://projecteuler.net/problem=12)&nbsp;&nbsp;&nbsp;&nbsp;[0014](https://projecteuler.net/problem=14)&nbsp;&nbsp;&nbsp;&nbsp;
[0021](https://projecteuler.net/problem=21)&nbsp;&nbsp;&nbsp;&nbsp;[0023](https://projecteuler.net/problem=23)&nbsp;&nbsp;&nbsp;&nbsp;[0030](https://projecteuler.net/problem=30)&nbsp;&nbsp;&nbsp;&nbsp;[0032](https://projecteuler.net/problem=32)&nbsp;&nbsp;&nbsp;&nbsp;[0033](https://projecteuler.net/problem=33)&nbsp;&nbsp;&nbsp;&nbsp;[0034](https://projecteuler.net/problem=34)&nbsp;&nbsp;&nbsp;&nbsp;[0038](https://projecteuler.net/problem=38)&nbsp;&nbsp;&nbsp;&nbsp;[0039](https://projecteuler.net/problem=39)&nbsp;&nbsp;&nbsp;&nbsp;[0043](https://projecteuler.net/problem=43)&nbsp;&nbsp;&nbsp;&nbsp;[0044](https://projecteuler.net/problem=44)&nbsp;&nbsp;&nbsp;&nbsp;
[0045](https://projecteuler.net/problem=45)&nbsp;&nbsp;&nbsp;&nbsp;[0046](https://projecteuler.net/problem=46)&nbsp;&nbsp;&nbsp;&nbsp;[0047](https://projecteuler.net/problem=47)&nbsp;&nbsp;&nbsp;&nbsp;[0048](https://projecteuler.net/problem=48)&nbsp;&nbsp;&nbsp;&nbsp;[0051](https://projecteuler.net/problem=51)&nbsp;&nbsp;&nbsp;&nbsp;[0052](https://projecteuler.net/problem=52)&nbsp;&nbsp;&nbsp;&nbsp;[0057](https://projecteuler.net/problem=57)&nbsp;&nbsp;&nbsp;&nbsp;[0059](https://projecteuler.net/problem=59)&nbsp;&nbsp;&nbsp;&nbsp;[0062](https://projecteuler.net/problem=62)&nbsp;&nbsp;&nbsp;&nbsp;[0063](https://projecteuler.net/problem=63)&nbsp;&nbsp;&nbsp;&nbsp;
[0071](https://projecteuler.net/problem=71)&nbsp;&nbsp;&nbsp;&nbsp;[0073](https://projecteuler.net/problem=73)&nbsp;&nbsp;&nbsp;&nbsp;[0074](https://projecteuler.net/problem=74)&nbsp;&nbsp;&nbsp;&nbsp;[0091](https://projecteuler.net/problem=91)&nbsp;&nbsp;&nbsp;&nbsp;[0092](https://projecteuler.net/problem=92)&nbsp;&nbsp;&nbsp;&nbsp;[0093](https://projecteuler.net/problem=93)&nbsp;&nbsp;&nbsp;&nbsp;[0098](https://projecteuler.net/problem=98)&nbsp;&nbsp;&nbsp;&nbsp;[0099](https://projecteuler.net/problem=99)&nbsp;&nbsp;&nbsp;&nbsp;
### 素数
[0003](https://projecteuler.net/problem=3)&nbsp;&nbsp;&nbsp;&nbsp;[0007](https://projecteuler.net/problem=7)&nbsp;&nbsp;&nbsp;&nbsp;[0010](https://projecteuler.net/problem=10)&nbsp;&nbsp;&nbsp;&nbsp;[0027](https://projecteuler.net/problem=27)&nbsp;&nbsp;&nbsp;&nbsp;[0035](https://projecteuler.net/problem=35)&nbsp;&nbsp;&nbsp;&nbsp;[0037](https://projecteuler.net/problem=37)&nbsp;&nbsp;&nbsp;&nbsp;[0041](https://projecteuler.net/problem=41)&nbsp;&nbsp;&nbsp;&nbsp;[0049](https://projecteuler.net/problem=49)&nbsp;&nbsp;&nbsp;&nbsp;[0050](https://projecteuler.net/problem=50)&nbsp;&nbsp;&nbsp;&nbsp;[0058](https://projecteuler.net/problem=58)&nbsp;&nbsp;&nbsp;&nbsp;
[0060](https://projecteuler.net/problem=60)&nbsp;&nbsp;&nbsp;&nbsp;[0077](https://projecteuler.net/problem=77)&nbsp;&nbsp;&nbsp;&nbsp;[0087](https://projecteuler.net/problem=87)&nbsp;&nbsp;&nbsp;&nbsp;[0095](https://projecteuler.net/problem=95)&nbsp;&nbsp;&nbsp;&nbsp;
### 毕达哥拉斯三元组
[0009](https://projecteuler.net/problem=9)&nbsp;&nbsp;&nbsp;&nbsp;[0075](https://projecteuler.net/problem=75)&nbsp;&nbsp;&nbsp;&nbsp;[0086](https://projecteuler.net/problem=86)&nbsp;&nbsp;&nbsp;&nbsp;
### 高精度计算
[0013](https://projecteuler.net/problem=13)&nbsp;&nbsp;&nbsp;&nbsp;[0016](https://projecteuler.net/problem=16)&nbsp;&nbsp;&nbsp;&nbsp;[0020](https://projecteuler.net/problem=20)&nbsp;&nbsp;&nbsp;&nbsp;[0025](https://projecteuler.net/problem=25)&nbsp;&nbsp;&nbsp;&nbsp;[0026](https://projecteuler.net/problem=26)&nbsp;&nbsp;&nbsp;&nbsp;[0029](https://projecteuler.net/problem=29)&nbsp;&nbsp;&nbsp;&nbsp;[0055](https://projecteuler.net/problem=55)&nbsp;&nbsp;&nbsp;&nbsp;[0056](https://projecteuler.net/problem=56)&nbsp;&nbsp;&nbsp;&nbsp;[0080](https://projecteuler.net/problem=80)&nbsp;&nbsp;&nbsp;&nbsp;
### 动态规划
[0015](https://projecteuler.net/problem=15)&nbsp;&nbsp;&nbsp;&nbsp;[0018](https://projecteuler.net/problem=18)&nbsp;&nbsp;&nbsp;&nbsp;[0031](https://projecteuler.net/problem=31)&nbsp;&nbsp;&nbsp;&nbsp;[0040](https://projecteuler.net/problem=40)&nbsp;&nbsp;&nbsp;&nbsp;[0067](https://projecteuler.net/problem=67)&nbsp;&nbsp;&nbsp;&nbsp;[0076](https://projecteuler.net/problem=76)&nbsp;&nbsp;&nbsp;&nbsp;[0077](https://projecteuler.net/problem=77)&nbsp;&nbsp;&nbsp;&nbsp;[0081](https://projecteuler.net/problem=81)&nbsp;&nbsp;&nbsp;&nbsp;[0082](https://projecteuler.net/problem=82)&nbsp;&nbsp;&nbsp;&nbsp;[0085](https://projecteuler.net/problem=85)&nbsp;&nbsp;&nbsp;&nbsp;
### 模拟
[0017](https://projecteuler.net/problem=17)&nbsp;&nbsp;&nbsp;&nbsp;[0019](https://projecteuler.net/problem=19)&nbsp;&nbsp;&nbsp;&nbsp;[0022](https://projecteuler.net/problem=22)&nbsp;&nbsp;&nbsp;&nbsp;[0028](https://projecteuler.net/problem=28)&nbsp;&nbsp;&nbsp;&nbsp;[0042](https://projecteuler.net/problem=42)&nbsp;&nbsp;&nbsp;&nbsp;[0054](https://projecteuler.net/problem=54)&nbsp;&nbsp;&nbsp;&nbsp;[0058](https://projecteuler.net/problem=58)&nbsp;&nbsp;&nbsp;&nbsp;[0084](https://projecteuler.net/problem=84)&nbsp;&nbsp;&nbsp;&nbsp;[0089](https://projecteuler.net/problem=89)&nbsp;&nbsp;&nbsp;&nbsp;[0093](https://projecteuler.net/problem=93)&nbsp;&nbsp;&nbsp;&nbsp;
[0096](https://projecteuler.net/problem=96)&nbsp;&nbsp;&nbsp;&nbsp;
### 排列组合
[0024](https://projecteuler.net/problem=24)&nbsp;&nbsp;&nbsp;&nbsp;[0041](https://projecteuler.net/problem=41)&nbsp;&nbsp;&nbsp;&nbsp;[0053](https://projecteuler.net/problem=53)&nbsp;&nbsp;&nbsp;&nbsp;
### 进制转换
[0036](https://projecteuler.net/problem=36)&nbsp;&nbsp;&nbsp;&nbsp;
### 搜索
[0060](https://projecteuler.net/problem=60)&nbsp;&nbsp;&nbsp;&nbsp;[0061](https://projecteuler.net/problem=61)&nbsp;&nbsp;&nbsp;&nbsp;[0079](https://projecteuler.net/problem=79)&nbsp;&nbsp;&nbsp;&nbsp;[0086](https://projecteuler.net/problem=86)&nbsp;&nbsp;&nbsp;&nbsp;[0088](https://projecteuler.net/problem=88)&nbsp;&nbsp;&nbsp;&nbsp;[0090](https://projecteuler.net/problem=90)&nbsp;&nbsp;&nbsp;&nbsp;[0095](https://projecteuler.net/problem=95)&nbsp;&nbsp;&nbsp;&nbsp;[0096](https://projecteuler.net/problem=96)&nbsp;&nbsp;&nbsp;&nbsp;
### 连分数
[0064](https://projecteuler.net/problem=64)&nbsp;&nbsp;&nbsp;&nbsp;[0065](https://projecteuler.net/problem=65)&nbsp;&nbsp;&nbsp;&nbsp;[0066](https://projecteuler.net/problem=66)&nbsp;&nbsp;&nbsp;&nbsp;
### 丢番图等式
[0066](https://projecteuler.net/problem=66)&nbsp;&nbsp;&nbsp;&nbsp;[0094](https://projecteuler.net/problem=94)&nbsp;&nbsp;&nbsp;&nbsp;[0100](https://projecteuler.net/problem=100)&nbsp;&nbsp;&nbsp;&nbsp;
### 推理
[0068](https://projecteuler.net/problem=68)&nbsp;&nbsp;&nbsp;&nbsp;
### 欧拉函数
[0069](https://projecteuler.net/problem=69)&nbsp;&nbsp;&nbsp;&nbsp;[0070](https://projecteuler.net/problem=70)&nbsp;&nbsp;&nbsp;&nbsp;[0072](https://projecteuler.net/problem=72)&nbsp;&nbsp;&nbsp;&nbsp;
### 分拆函数
[0078](https://projecteuler.net/problem=78)&nbsp;&nbsp;&nbsp;&nbsp;
### 拓扑排序
[0079](https://projecteuler.net/problem=79)&nbsp;&nbsp;&nbsp;&nbsp;
### 最短路径
[0083](https://projecteuler.net/problem=83)&nbsp;&nbsp;&nbsp;&nbsp;
### 马尔科夫链
[0084](https://projecteuler.net/problem=84)&nbsp;&nbsp;&nbsp;&nbsp;
### 积和数
[0088](https://projecteuler.net/problem=88)&nbsp;&nbsp;&nbsp;&nbsp;
### 海伦公式
[0094](https://projecteuler.net/problem=94)&nbsp;&nbsp;&nbsp;&nbsp;
### 状态压缩
[0096](https://projecteuler.net/problem=96)&nbsp;&nbsp;&nbsp;&nbsp;
### 快速求幂
[0097](https://projecteuler.net/problem=97)&nbsp;&nbsp;&nbsp;&nbsp;
